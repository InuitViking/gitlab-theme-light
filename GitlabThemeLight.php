<?php


namespace Imms\GitlabThemeLight;

use Imms\Classes\Bootstrapper;
use Imms\Classes\Route;
use Imms\Classes\Theme;
use Imms\Interfaces\ThemeInterface;
use League\Plates\Engine;

class GitlabThemeLight implements ThemeInterface {
    private Engine $templates;
    private string $rootDir;
    private array|false $ini;

    public function __construct() {
        $this->rootDir = Bootstrapper::rootDirectory();
        $this->ini = Bootstrapper::getIni();
        $baseThemePath = Theme::getBaseThemePath($this->ini['app']['theme']);
        $config = Theme::getThemeConfig($_COOKIE['IMMSTheme']);
        $data = [
            'metaDescription' => $this->ini['app']['meta_description'],
            'pageTitle' => Route::getTitle(),
            'sideMenu' => Theme::prerender("$baseThemePath/templates/partials/sideMenu.php"),
            'highlightjsTheme' => $config['regions']['highlightjsTheme'],
            'stylesheet' => $config['regions']['stylesheet'],
        ];
        self::setEngine();
        $this->templates->addData($data, 'partials/header');
    }

    #[\Override] public function setEngine(): void {
        $templateDir = $this->rootDir . '/src/themes/imms/gitlab-theme-light/templates';
        $this->templates = new Engine($templateDir);
    }

    #[\Override] public function getEngine(): Engine {
        return $this->templates;
    }
}
